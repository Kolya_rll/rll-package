<?php

namespace rll\rllWorker;


class Greeting
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
